# JsNgxRockets

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.0.8.

## App Refactoring 🚀

Your task today:

* Take a look and try to understand what this app is doing. Try to get it running.
* If there are any problems, try to fix them.
* If there is bad code, feel free to refactor it.
* Please tell us what you are doing and what your ideas are, so that we can follow you.
* To run this app you need at best Node.js version `v21.5.0` and npm version `10.2.4`.

A couple of key points:

* This is not a test! There is no "master" solution. We just want to get an overall picture of you working with code.
* Just pretend it is your first working day and this is your task. You can do everything you would normally do (including use Google, Stackoverflow or ask us for help).
* This is supposed to be done in casual conversation, relax and tell us your opinion about the questions.
* We do pair programming here and this interview is not different, so if there is anything you need help with, please ask.
* You can use your favorite IDE.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
