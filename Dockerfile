FROM node:14.15 as build

RUN mkdir -p /app
WORKDIR /app

COPY . .
RUN npm ci && npm build


FROM nginx:latest

RUN mkdir -p /app
COPY --from=build /app/dist/js-ngx-rockets/ /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
